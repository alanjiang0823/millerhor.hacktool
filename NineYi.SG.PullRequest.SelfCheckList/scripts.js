// ==UserScript==
// @name         NineYi.SG.PullRequest.SelfCheckList
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  try to take over the world!
// @author       MillerHor
// @match        https://bitbucket.org/nineyi/*/pull-requests/*
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// ==/UserScript==
(function() {
    'use strict';

    $(function(){
        $.getScript('https://bitbucket.org/MillerHor/millerhor.hacktool/raw/master/NineYi.SG.PullRequest.SelfCheckList/resource.js', function(data){
            var nameSpace = 'nineyi_sg_pr_self_check_list';
            var ruleList = nineYi.sg.pr.selfCheckList.list,
                ruleVersion = nineYi.sg.pr.selfCheckList.version;

            var checkListBoard = $('<div style="position:fixed;bottom:100px;left:10px;background:#FFF;z-index:2999;border:1px solid rgb(223, 225, 230);border-radius:3px;padding:5px" />'),
                miniferButton = $('<div style="border:1px solid #DFE1E6;border-radius:3px;width:18px;height:18px;padding:1px;text-align:center;vertical-align:middle;float:right;cursor:pointer">-</div>');

            miniferButton.click(function(){
                if(checkListBoard.position().left < 0){
                    $(this).text('-');
                    checkListBoard.animate({left:'10px'});
                }
                else{
                    var offsetLeft = 0 - checkListBoard.width() + 18;
                    checkListBoard.animate({left:offsetLeft + 'px'});
                    $(this).text('+');
                }
            });
            //// 最小化
            checkListBoard.append(miniferButton);
            //// title
            checkListBoard.append('<h1 class="app-header--heading"><span class="pull-request-title">PR Check List v' + ruleVersion + '</span></h1>');
            //// 分隔線
            checkListBoard.append('<div style="border-bottom:1px solid #DFE1E6;margin:5px 0" />');

            $.each(ruleList, function(idx){
                var rule = this;
                var id = nameSpace + '_' + idx;
                var ruleCheckBox = $('<input type="checkbox" id="' + id + '" name="' + rule + '" value="' + rule + '" />');
                var ruleLable = $('<label for="' + id + '" class="description">' + rule + '</label>');
                var ruleItem = $('<div style="padding-bottom:2px"/>');

                ruleItem.append(ruleCheckBox);
                ruleItem.append(ruleLable);
                checkListBoard.append(ruleItem);
            });

            var confirmButtom = $('<button>確認完畢</button>').click(function(){
                var checkedItem = [];
                checkListBoard.find('input').each(function(){
                    var checkItem = $(this);
                    if(checkItem.prop('type') != 'checkbox'){return;}
                    console.log(checkItem.prop('checked'));
                    if(checkItem.prop('checked') === false){return;}
                    checkedItem.push(checkItem.val());
                });
                console.log(checkedItem);
                //postComment('* ' + checkedItem.join('\n* ') + '檢查完畢');
            });

            //// 分隔線
            checkListBoard.append('<div style="border-bottom:1px solid #DFE1E6;margin:5px 0" />');
            //// 確認按鈕
            checkListBoard.append(confirmButtom);


            $(document.body).append(checkListBoard);

            function postComment(message){
                //// /nineyi/nineyi.webstore.mobilewebmall/pull-requests/8638/feature-vsts12185-fix-printcode/diff
                var localPath = location.pathname.split('/');

                //// https://bitbucket.org/!api/1.0/repositories/nineyi/nineyi.webstore.mobilewebmall/pullrequests/8638/comments/
                var targetPath = ['https://bitbucket.org/!api/1.0/repositories' , localPath[1], localPath[2], 'pullrequests', localPath[4], 'comments'].join('/');

                //// jQuery.post( url [, data ] [, success ] [, dataType ] )
                var postContent = {"content":message};
                var antiCSRFKey = $(document.getElementById('image-upload-template').innerText).find('[name="csrfmiddlewaretoken"]').val();

                $.ajax(targetPath, {
                    headers:{
                        'Accept':'application/json, text/javascript, */*; q=0.01',
                        'x-csrftoken' : antiCSRFKey
                    },
                    method: 'POST',
                    data: postContent,
                    complete : function(){
                        location.href = location.href;
                    }
                });
            }
        });

        // Your code here...
    });
})();