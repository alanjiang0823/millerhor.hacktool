var nineYi = {
  sg:{
    pr:{
      selfCheckList:{
        version:'1.0',
        list:['SonarCube掃描已執行', 
          'SonarCube建議修改項目已調整完畢',
          '沒有衝突',
          '目標Branch正確', 
          '本次修改需求的Release時間正確', 
          'AppSettings.Config各環境都有新增/修改', 
          'AppSettings.Config無機敏性資料(ApiKey、EncryptKey...等)', 
          'EDMX只修改本次需求範圍內的欄位', 
          '有調整Cache(Redis、Memory、File)內容，應更新Cache key', 
          '程式碼只修改本次需求範圍內容'
        ]
      }
    }
  }
};